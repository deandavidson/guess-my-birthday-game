from random import randint

name = input("Hello! What is your name?")

for guess_number in range(1, 6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess", guess_number, ": were you born in", month_number, "/", year_number, "?")
    response = input("Yes or no:")
    if response == "yes":
        print("I knew it!! Goodbye.")
        exit()
    elif guess_number == 5:
        print("I give up, this is too hard for my little computer brain!")
    else:
        print("Darn it!!! Lemme try again...")
